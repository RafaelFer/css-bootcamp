# CSS-Bootcamp #

High level topics for this bootcamp

### Topics  ###

* Basics 
* Advanced
* HTML 5 + CSS 3
* CSS Animation
* Pitfalls

### Basics  ###

* Basic HTML 
* Basic CSS 
* Classes 
* Wrappers 
* Units (px, em, rem)

### Advanced ###

* CSS Selectors 
* Media Queries  

### HTML 5 + CSS 3 ###

* TBD 

### Frameworks ###

* Bootstrap
* Material Design
* Skeleton

### CSS Animation ###

* trasition
* types
* how to stop an animation
* ng-Animate

### Pitfalls ###

* float
* clear both

### Other topics ###

* SASS
* LESS 
